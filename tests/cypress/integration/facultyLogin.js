describe('Faculty Student', function() {
    it('sucessfull', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/facultylogin')
        cy.get('#fid').type('101')
        cy.get('#pass').type('1234')
        cy.get('#submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/welcomefaculty.php')
    })

    it('Error id', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/facultylogin')
        cy.get('#fid').type('202')
        cy.get('#pass').type('1234')
        cy.get('#submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/loginlinkfaculty')
    })

    it('Error password', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/facultylogin')
        cy.get('#fid').type('101')
        cy.get('#pass').type('test')
        cy.get('#submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/loginlinkfaculty')
    })
})