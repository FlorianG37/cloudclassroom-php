describe('take assessment', function() {
    it('failed not connected', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/takeassessment.php')
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/studentlogin')
    })

    it('sucessfull', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/studentlogin')
        cy.get('#student_name').type('harsh@ics.com')
        cy.get('#student_password').type('1234')
        cy.get('#student_submit').click()
        cy.get('#take_assessment').click()
        cy.get('[href="takeassessment2.php?exid=4"]').click()
        cy.get('[name="Q1"]').type("123123123213")
        cy.get('[name="Q2"]').type("123123123213")
        cy.get('[name="Q3"]').type("123123123213")
        cy.get('[name="Q4"]').type("123123123213")
        cy.get('[name="Q5"]').type("123123123213")
        cy.get('[name="done"]').click()
        cy.contains('Assessment Have Submited')
    })
})