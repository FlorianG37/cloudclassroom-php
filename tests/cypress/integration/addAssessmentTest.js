describe('Add assesment', function() {
    it('failed not connected', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/addassessment.php')
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/facultylogin')
    })

    it('sucessfull', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/facultylogin')
        cy.get('#fid').type('101')
        cy.get('#pass').type('1234')
        cy.get('#submit').click()
        cy.visit('http://localhost/cloudclassroom-php/sources/addassessment.php')
        cy.get('#AssessmentName').type("test")
        cy.get('#Q1').type("this is a test question ?")
        cy.get('#Q2').type("this is a test question ?")
        cy.get('#Q3').type("this is a test question ?")
        cy.get('#Q4').type("this is a test question ?")
        cy.get('#Q5').type("this is a test question ?")
        cy.get('#submit').click()
        cy.contains('Assessment added Sucessfully.')
    })
})