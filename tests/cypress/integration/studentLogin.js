describe('Login Student', function() {
    it('sucessfull', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/studentlogin')
        cy.get('#student_name').type('harsh@ics.com')
        cy.get('#student_password').type('1234')
        cy.get('#student_submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/welcomestudent.php')
    })

    it('Error id', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/studentlogin')
        cy.get('#student_name').type('test@ics.com')
        cy.get('#student_password').type('1234')
        cy.get('#student_submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/loginlinkstudent')
    })

    it('Error password', function() {
        cy.visit('http://localhost/cloudclassroom-php/sources/studentlogin')
        cy.get('#student_name').type('harsh@ics.com')
        cy.get('#student_password').type('test')
        cy.get('#student_submit').click()
        cy.url().should('eq', 'http://localhost/cloudclassroom-php/sources/loginlinkstudent')
    })
})